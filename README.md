# diymatrix

Do-it-yourself  desk LED matrix build.

I've always wanted to have my own LED matrix at home, just to play with it and be able to build some
cool stuff. This repository documents how I did that, and how you can do it as well at home.

# Materials and Cost

It is fairly inexpensive to build your own, it costs about 200€, depending on the size and how cheap
you can source the parts. You will need:

- 16xMAX7219 4-panel LED Matrix Modules
- Arduino (any works)
- Some wire
- Mounting screws
- Wood
- Meanwell LPV-60-5

You will also need a soldering station to put things together.

# Build

TODO.

# Firmware

The folder `led_matrix` contains firmware that you can flash onto your
Arduino for the firmware. This is customizable to be able to talk to panels of different sizes, if
you want to build a smaller or larger one than me.

# Master software

Depending on how you want to build things up, you can have the arduino be controlled via
a PC or via a raspberry pi. This repository additionally contains a software written in 
Rust that accepts commands from the network and controls the LED panel

# Client software

Additionally, this repo contains some example clients I've written.

# License

MIT License.
