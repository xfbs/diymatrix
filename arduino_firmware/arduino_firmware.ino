#include <SPI.h>

// what pins are the chip select pins attached?
const uint8_t bank_cs_pins[] = {10, 9, 8, 7, 6, 5, 4, 3, 0};
// how long are the panels (assuming all panels same length)
const uint8_t bank_length = 16;

// SPI pins
#define SPI_MOSI 11
#define SPI_MISO 12
#define SPI_CLK  13

// max7219 commands
#define MAX7219_NOP 0x00
#define MAX7219_ROW(row) (0x01 + (row))
#define MAX7219_DECODE_MODE 0x09
#define MAX7219_BRIGHTNESS 0x0a
#define MAX7219_SCAN_LIMIT 0x0b
#define MAX7219_SHUTDOWN 0x0c
#define MAX7219_TEST 0x0f

void bank_enable(uint8_t index) {
  digitalWrite(bank_cs_pins[index], LOW);
}

void bank_disable(uint8_t index) {
  digitalWrite(bank_cs_pins[index], HIGH);
}

void bank_setup(uint8_t index) {
  pinMode(bank_cs_pins[index], OUTPUT);
  digitalWrite(bank_cs_pins[index], HIGH);
}

void banks_setup() {
  for(size_t i = 0; bank_cs_pins[i]; i++) {
    bank_setup(i);
  }
}

void banks_foreach(void (*fn)(uint8_t index)) {
  for(size_t i = 0; bank_cs_pins[i]; i++) {
    fn(i);
  }
}

void banks_set(uint8_t state) {
  for(size_t i = 0; i < 8; i++) {
    if((state >> i) & 1) {
      bank_enable(i);
    } else {
      bank_disable(i);
    }
  }
}

void matrix_send(uint8_t addr, uint8_t value) {
  SPI.transfer(addr);
  SPI.transfer(value);
}

void maxTransferCMD(uint8_t address, uint8_t value) {  
  //bank_enable(0);
  //bank_enable(1);
  banks_foreach(bank_enable);
  SPI.transfer(address);      // Send address.
  SPI.transfer(value);        // Send the value.
  //SPI.transfer(address);      // Send address. 
  //SPI.transfer(value);        // Send the value.
  //bank_disable(0);
  //bank_disable(1);
  banks_foreach(bank_disable);
}

void matrix_setup() {
  //banks_foreach(bank_enable);
  maxTransferCMD(MAX7219_TEST, 0x00);        // Finish test mode.
  maxTransferCMD(MAX7219_DECODE_MODE, 0x00); // Disable BCD mode. 
  maxTransferCMD(MAX7219_BRIGHTNESS, 0x00);  // Use lowest intensity. 
  maxTransferCMD(MAX7219_SCAN_LIMIT, 0x0f);  // Scan all digits.
  maxTransferCMD(MAX7219_SHUTDOWN, 0x01);    // Turn on chip.

  for(size_t num = 0; num < bank_length; num++) {
    for(size_t row = 0; row < 8; row++) {
      maxTransferCMD(MAX7219_ROW(row), 0);
    }
  }

  delay(10);
}

void maxTransferDATA(uint8_t address, uint8_t value, uint8_t v2) {  
uint8_t i;
  bank_enable(0);
  bank_enable(1);  

  SPI.transfer(address);      // Send address.
  SPI.transfer(value);        // Send the value.
  //SPI.transfer(address);      // Send address. 
  //SPI.transfer(v2);           // Send the value.

  bank_disable(0);
  bank_disable(1);
}

void setup() {
  // use high serial data rate
  Serial.begin(115200);

  // initialize banks
  banks_foreach(bank_setup);
  banks_foreach(bank_disable);

  //SPI.setClockDivider(SPI_CLOCK_DIV128);

  // initialize spi
  SPI.setBitOrder(MSBFIRST);
  SPI.begin();
  //SPISettings settings(1000000, MSBFIRST, SPI_MODE0);
  //SPI.beginTransaction(settings);

  delay(1);

  // init matrix
  matrix_setup();
}

enum parse_state {
  STATE_BANKS,
  STATE_ADDRESS,
  STATE_VALUE,
};

parse_state state = STATE_BANKS;

uint8_t banks_state = 0;
uint8_t address = 0;

void loop() {
  if(!Serial.available()) {
    //Serial.println("eek");
    return;
  }

  int input = Serial.read();
  if(input < 0) {
    return;
  }

  //Serial.print("I got: ");
  //Serial.print(input, DEC);
  //Serial.print(" state ");
  //Serial.println(state, DEC);


  switch(state) {
    case STATE_BANKS:
      banks_state = input;
      state = STATE_ADDRESS;
      break;
    case STATE_ADDRESS:
      address = input;
      state = STATE_VALUE;
      break;
    case STATE_VALUE:
      //banks_foreach(bank_enable);
      banks_set(banks_state);
      matrix_send(address, input);
      banks_foreach(bank_disable);
      state = STATE_BANKS;
      break;
    default:
      state = STATE_BANKS;
      break;
  }

  /*
   for(uint8_t row = 0; row < 8; row++) {      
      //maxTransferDATA(row + 1, 1 << row, 0);             
      //maxTransferCMD(MAX7219_BRIGHTNESS, (row % 2) ? 0 : 255);
      banks_foreach(bank_disable);

      
      matrix_send(MAX7219_NOP, 0);
      matrix_send(MAX7219_ROW(row), 2);
      matrix_send(MAX7219_ROW(row), 2);
      bank_enable(1);
      matrix_send(MAX7219_ROW(row), 2);
      bank_disable(1);

      matrix_send(MAX7219_ROW(row), 1);
      matrix_send(MAX7219_ROW(row), 1);
      matrix_send(MAX7219_ROW(row), 1);
      bank_enable(0);
      matrix_send(MAX7219_ROW(row), 1);
      bank_disable(0);

      delay(1000);
   }
   */
}
