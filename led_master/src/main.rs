use bitvec::prelude::*;
use rand::Rng;
use std::net::SocketAddr;
use std::path::PathBuf;
use std::time::Duration;
use structopt::StructOpt;
use tokio::io::{AsyncWrite, AsyncWriteExt, AsyncReadExt, AsyncRead};
use tokio::net::{TcpStream, TcpListener, UdpSocket};
use tokio::sync::Mutex;
use tokio::time::MissedTickBehavior;
use std::sync::Arc;
use std::io::Cursor;
use tokio_serial::{SerialStream, SerialPortBuilderExt};
use itertools::Itertools;
use bitvec::order::Lsb0;
use log::*;

const BYTE_BITS: usize = 8;
const BLOCK_COLS: usize = 8;
const BLOCK_ROWS: usize = 8;

#[derive(StructOpt, Debug, Clone)]
struct Options {
    /// Which serial port to use
    #[structopt(long, short)]
    serial_port: String,
    #[structopt(long, short)]
    baud: u32,
    #[structopt(long, short)]
    listen: Vec<SocketAddr>,
    #[structopt(long, short)]
    udp: Vec<SocketAddr>,
    #[structopt(long, short="y")]
    rows: usize,
    #[structopt(long, short="x")]
    cols: usize,
    #[structopt(long, short, default_value="30")]
    fps: f32,
    #[structopt(long, short)]
    wait: bool,
    #[structopt(long, short)]
    /// How many seconds to retry to get a connection to the display before failing
    retry: bool,
    #[structopt(long, default_value="0")]
    brightness: u8,
    #[structopt(long, use_delimiter = true)]
    brightness_map: Vec<u8>,
}

struct Update {
    pos: (usize, usize),
    size: (usize, usize),
    data: Vec<BitVec<Lsb0, u8>>,
}

impl Update {
    pub async fn parse<T: AsyncRead + Unpin>(mut stream: T) -> Result<Update, anyhow::Error> {
        let pos_x = stream.read_u8().await? as usize;
        let pos_y = stream.read_u8().await? as usize;
        let dim_x = stream.read_u8().await? as usize;
        let dim_y = stream.read_u8().await? as usize;
        let len = bits_to_bytes((dim_x * dim_y).into());

        let mut data = vec![0; len];
        let data = if len > 0 {
            stream.read_exact(&mut data).await?;
            data.chunks(bits_to_bytes(dim_y)).map(|c| BitVec::from_slice(c).unwrap()).collect()
        } else {
            info!("Got null size from client");
            Vec::new()
        };

        Ok(Update {
            pos: (pos_x, pos_y),
            size: (dim_x, dim_y),
            data,
        })
    }
}

struct State {
    size: (usize, usize),
    data: Vec<BitVec<Lsb0, u8>>,
    dirty: Vec<BitVec<Lsb0, u8>>,
}

fn round_to_bits(num: usize) -> usize {
    BYTE_BITS * ((num + BYTE_BITS - 1) / BYTE_BITS)
}

fn bits_to_bytes(num: usize) -> usize {
    (num + BYTE_BITS - 1) / BYTE_BITS
}

impl State {
    pub fn new(rows: usize, cols: usize) -> Self {
        State {
            size: (rows, cols),
            data: vec![bitvec![Lsb0, u8; 0; cols]; rows],
            dirty: vec![bitvec![Lsb0, u8; 1; cols]; rows],
        }
    }

    pub fn set(&mut self, row: usize, col: usize, value: bool) -> Result<(), ()> {
        let data = self.data.get_mut(row).and_then(|r| r.get_mut(col));
        let dirty = self.dirty.get_mut(row).and_then(|r| r.get_mut(col));

        if let (Some(data), Some(dirty)) = (data, dirty) {
            if *data != value {
                data.set(value);
                dirty.set(true);
            }
            Ok(())
        } else {
            Err(())
        }
    }

    pub fn get(&mut self, row: usize, col: usize) -> Option<bool> {
        self.data
            .get(row)
            .and_then(|r| r.get(col).as_deref().map(|r| *r))
    }

    pub fn apply(&mut self, update: &Update) -> Result<(), anyhow::Error> {
        for x in 0..update.size.0 {
            for y in 0..update.size.1 {
                self.set(update.pos.0 + x, update.pos.1 + y, *update.data[x].get(y).unwrap());
            }
        }

        Ok(())
    }

    pub async fn update(&mut self, connection: &mut SerialStream) -> Result<(), std::io::Error> {
        for row in 0..self.size.0 {
            let bank = row / BLOCK_ROWS;
            let bank_row = (row % BLOCK_ROWS) as u8;
            let dirty: Vec<_> = self.dirty[row].as_bitslice().chunks(BLOCK_COLS).map(|b| b.any()).collect();

            // this row has dirty blocks
            if dirty.iter().any(|e| *e) {
                let data = self.data[row].as_raw_slice();
                for (i, block_data) in data.iter().enumerate().rev() {
                    if i == 0 {
                        let msg = [1 << bank, bank_row + 1, *block_data];
                        debug!("sending {:?}", msg);
                        connection.write(&msg).await?;
                    } else {
                        let msg = [0, bank_row + 1, *block_data];
                        debug!("sending {:?}", msg);
                        connection.write(&msg).await?;
                    }
                }

                // reset
                self.dirty[row].iter_mut().for_each(|e| e.set(false));
            }
        }

        Ok(())
    }

    /// Set all bits to dirty
    pub fn dirty_all(&mut self) {
        self.dirty.iter_mut().for_each(|row| row.iter_mut().for_each(|e| e.set(true)));
    }
}

async fn start_udp_listener(addr: SocketAddr, state: Arc<Mutex<State>>) -> Result<(), anyhow::Error> {
    let socket = UdpSocket::bind(addr).await?;
    info!("Bound to {} (UDP)", addr);

    let mut buf = vec![0u8; 1500];
    loop {
        let (len, addr) = socket.recv_from(&mut buf).await?;
        info!("Got UDP packet from {} len {}", addr, len);
        let message = &buf[0..len];
        let mut cursor = Cursor::new(message);
        match Update::parse(&mut cursor).await {
            Ok(update) => state.lock().await.apply(&update)?,
            Err(error) => error!("Error parsing packet from {}: {}", addr, error),
        }
    }

    Ok(())
}

async fn start_tcp_listener(addr: SocketAddr, state: Arc<Mutex<State>>) -> Result<(), anyhow::Error> {
    let listener = TcpListener::bind(addr).await?;
    info!("Bound to {}", addr);

    loop {
        let (socket, remote) = listener.accept().await?;
        info!("Connected {}", &remote);
        let state = state.clone();
        tokio::spawn(async move {
            let ret = handle_client(socket, state).await;
            info!("Disconnected {}", remote);
        });
    }
}

async fn handle_client<T: AsyncRead + Unpin>(mut socket: T, state: Arc<Mutex<State>>) -> Result<(), anyhow::Error> {
    loop {
        let update = Update::parse(&mut socket).await?;
        state.lock().await.apply(&update)?;
    }

    Ok(())
}

async fn connect_display(options: Arc<Options>, state: Arc<Mutex<State>>) -> Result<(), std::io::Error> {
    // connect to serial port
    let mut port = tokio_serial::new(&options.serial_port, options.baud)
        .timeout(Duration::from_millis(10))
        .open_async()?;
    port.set_exclusive(true)?;

    // wait for initialisation to complete
    std::thread::sleep(Duration::from_millis(2000));

    info!("Connected to matrix");

    for row in 0..options.rows {
        for col in 0..options.cols {
            port.write(&[1 << row, 0x0a, options.brightness + options.brightness_map.get(row).unwrap_or(&0)]).await?;
        }
    }

    // draw frames
    let mut interval = tokio::time::interval(Duration::from_secs_f32(1.0 / options.fps));
    interval.set_missed_tick_behavior(MissedTickBehavior::Delay);
    loop {
        interval.tick().await;
        state.lock().await.update(&mut port).await?;
    }
}

#[tokio::main]
async fn main() -> Result<(), anyhow::Error> {
    env_logger::init();
    let options = Options::from_args();
    let options = Arc::new(options);
    debug!("options: {:?}", options);

    // create new state
    let state = Arc::new(Mutex::new(State::new(options.rows * BLOCK_ROWS, options.cols * BLOCK_COLS)));

    // start listening to clients
    for addr in &options.listen {
        let state = state.clone();
        let addr = addr.clone();
        tokio::spawn(async move {
            match start_tcp_listener(addr, state).await {
                Err(e) => error!("Error listening on {}: {}", addr, e),
                _ => {},
            }
        });
    }

    // start listening to clients
    for addr in &options.udp {
        let state = state.clone();
        let addr = addr.clone();
        tokio::spawn(async move {
            match start_udp_listener(addr, state).await {
                Err(e) => error!("Error listening on {}: {}", addr, e),
                _ => {},
            }
        });
    }

    // connect to display
    loop {
        let ret = connect_display(options.clone(), state.clone()).await;

        match &ret {
            Ok(()) => break,
            Err(e) => error!("Error connecting to matrix: {}", e),
        }

        if !options.retry {
            ret?;
        }

        // at this point, we will retry, but wait one second between retries
        tokio::time::sleep(Duration::from_secs(1)).await;

        // dirty all pixels, this causes all of them to be retransmitted upon
        // reconnection.
        state.lock().await.dirty_all();
    }

    Ok(())
}
