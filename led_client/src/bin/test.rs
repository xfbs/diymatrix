use structopt::StructOpt;
use std::net::{TcpStream, SocketAddr};
use std::io::Write;
use led_client::Update;
use std::time::Duration;

#[derive(StructOpt)]
struct Options {
    host: SocketAddr,
    #[structopt(long, short, default_value="0")]
    col: usize,
    #[structopt(long, short, default_value="0")]
    row: usize,
}

fn main() -> Result<(), anyhow::Error> {
    let options = Options::from_args();
    let mut stream = TcpStream::connect(options.host)?;
    let mut update = Update::new();
    update.set_size(8, 8);
    update.set_position(options.row, options.col);

    loop {
        update.clear();
        update.set_bit(0, 0, true);
        update.set_bit(1, 1, true);
        update.set_bit(2, 2, true);
        update.set_bit(3, 3, true);
        update.set_bit(4, 4, true);
        update.set_bit(5, 5, true);
        update.set_bit(6, 6, true);
        update.set_bit(7, 7, true);
        stream.write(&update.to_message())?;

        std::thread::sleep(Duration::from_millis(200));

        update.clear();
        update.set_bit(0, 7, true);
        update.set_bit(1, 6, true);
        update.set_bit(2, 5, true);
        update.set_bit(3, 4, true);
        update.set_bit(4, 3, true);
        update.set_bit(5, 2, true);
        update.set_bit(6, 1, true);
        update.set_bit(7, 0, true);
        stream.write(&update.to_message())?;

        std::thread::sleep(Duration::from_millis(200));
    }

    Ok(())
}
