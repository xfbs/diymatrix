use structopt::StructOpt;
use std::net::{TcpStream, SocketAddr};
use std::io::Write;
use led_client::Update;
use std::time::Duration;
use rand::Rng;

#[derive(StructOpt)]
struct Options {
    host: SocketAddr,
    #[structopt(long, short, default_value="0")]
    col: usize,
    #[structopt(long, short, default_value="0")]
    row: usize,
    #[structopt(long, default_value="96")]
    cols: usize,
    #[structopt(long, default_value="40")]
    rows: usize,
    #[structopt(long, short)]
    fps: Option<f32>,
}

fn main() -> Result<(), anyhow::Error> {
    let options = Options::from_args();
    let mut stream = TcpStream::connect(options.host)?;
    let mut update = Update::new();
    let mut rng = rand::thread_rng();
    update.set_size(options.rows, options.cols);
    update.set_position(options.row, options.col);

    loop {
        for r in 0..options.rows {
            for c in 0..options.cols {
                update.set_bit(r, c, rng.gen());
            }
        }
        stream.write(&update.to_message())?;

        if let Some(fps) = options.fps {
            std::thread::sleep(Duration::from_secs_f32(1.0 / fps));
        } else {
            break;
        }
    }

    Ok(())
}
