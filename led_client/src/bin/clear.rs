use structopt::StructOpt;
use std::net::{TcpStream, SocketAddr};
use std::io::Write;
use led_client::Update;
use std::time::Duration;

#[derive(StructOpt)]
struct Options {
    host: SocketAddr,
    #[structopt(long, short, default_value="0")]
    col: usize,
    #[structopt(long, short, default_value="0")]
    row: usize,
    #[structopt(long, default_value="96")]
    cols: usize,
    #[structopt(long, default_value="40")]
    rows: usize,
}

fn main() -> Result<(), anyhow::Error> {
    let options = Options::from_args();
    let mut stream = TcpStream::connect(options.host)?;
    let mut update = Update::new();
    update.set_size(options.rows, options.cols);
    update.set_position(options.row, options.col);
    stream.write(&update.to_message())?;

    Ok(())
}
