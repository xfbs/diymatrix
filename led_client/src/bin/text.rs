use structopt::StructOpt;
use std::net::{TcpStream, SocketAddr};
use std::io::Write;
use led_client::Update;
use std::time::Duration;
use font8x8::UnicodeFonts;

#[derive(StructOpt)]
struct Options {
    host: SocketAddr,
    text: String,
    #[structopt(long, short, default_value="0")]
    col: usize,
    #[structopt(long, short, default_value="0")]
    row: usize,
}

fn main() -> Result<(), anyhow::Error> {
    let options = Options::from_args();
    let mut stream = TcpStream::connect(options.host)?;
    let mut update = Update::new();
    let len = options.text.chars().count();
    update.set_size(8, 8 * len);
    update.set_position(options.row, options.col);

    let mut fonts: Vec<fn(char) -> Option<[u8; 8]>> = Vec::new();
    fonts.push(|c| font8x8::unicode::BasicFonts::new().get(c));
    fonts.push(|c| font8x8::unicode::BlockFonts::new().get(c));
    fonts.push(|c| font8x8::unicode::BoxFonts::new().get(c));
    fonts.push(|c| font8x8::unicode::GreekFonts::new().get(c));
    fonts.push(|c| font8x8::unicode::HiraganaFonts::new().get(c));
    fonts.push(|c| font8x8::unicode::LatinFonts::new().get(c));
    fonts.push(|c| font8x8::unicode::MiscFonts::new().get(c));
    fonts.push(|c| font8x8::unicode::SgaFonts::new().get(c));

    for (i, c) in options.text.chars().enumerate() {
        if let Some(data) = fonts.iter().map(|f| f(c)).filter_map(|x| x).nth(0) {
            for (r, row) in data.iter().enumerate() {
                update.set_byte(7 - r, i, *row);
            }
        }
    }

    stream.write(&update.to_message())?;
    Ok(())
}
