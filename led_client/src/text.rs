use crate::Update;
use font8x8::UnicodeFonts;

pub fn text_to_update(text: &str) -> Update {
    let mut update = Update::new();
    let len = text.chars().count();
    update.set_size(8, 8 * len);

    let mut fonts: Vec<fn(char) -> Option<[u8; 8]>> = Vec::new();
    fonts.push(|c| font8x8::unicode::BasicFonts::new().get(c));
    fonts.push(|c| font8x8::unicode::BlockFonts::new().get(c));
    fonts.push(|c| font8x8::unicode::BoxFonts::new().get(c));
    fonts.push(|c| font8x8::unicode::GreekFonts::new().get(c));
    fonts.push(|c| font8x8::unicode::HiraganaFonts::new().get(c));
    fonts.push(|c| font8x8::unicode::LatinFonts::new().get(c));
    fonts.push(|c| font8x8::unicode::MiscFonts::new().get(c));
    fonts.push(|c| font8x8::unicode::SgaFonts::new().get(c));

    for (i, c) in text.chars().enumerate() {
        if let Some(data) = fonts.iter().map(|f| f(c)).filter_map(|x| x).nth(0) {
            for (r, row) in data.iter().enumerate() {
                update.set_byte(7 - r, i, *row);
            }
        }
    }

    update
}
