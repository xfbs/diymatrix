use serde::{Serialize, Deserialize};
use std::net::SocketAddr;
use std::path::PathBuf;
use structopt::StructOpt;
use tokio::net::TcpStream;
use tokio::sync::mpsc;
use tokio::io::AsyncWriteExt;
use led_client::Update;
use led_client::widgets::*;

#[tokio::main]
pub async fn main() -> Result<(), anyhow::Error> {
    let options = Options::from_args();
    let config_file = std::fs::read_to_string(options.config)?;
    let config: Config = toml::from_str(&config_file)?;
    let mut connection = TcpStream::connect(options.server.or(config.screen.server).unwrap()).await?;
    let (send, mut recv) = tokio::sync::mpsc::channel(1024);

    // clear screen at startup
    let mut update = Update::new();
    update.set_size(config.screen.size.0, config.screen.size.1);
    let msg = update.to_message();
    send.send(msg).await;

    for widget in config.widgets {
        tokio::spawn(widget.run(send.clone()));
    }

    while let Some(msg) = recv.recv().await {
        connection.write(&msg).await?;
    }

    Ok(())
}
