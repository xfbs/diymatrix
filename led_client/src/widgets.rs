use serde::{Serialize, Deserialize};
use std::net::SocketAddr;
use std::path::PathBuf;
use std::time::Duration;
use structopt::StructOpt;
use tokio::net::TcpStream;
use tokio::sync::mpsc;
use tokio::io::AsyncWriteExt;
use tokio::time::MissedTickBehavior;
use crate::Update;
use crate::text::text_to_update;

#[derive(StructOpt, Clone, Debug)]
pub struct Options {
    #[structopt(long, short)]
    pub server: Option<SocketAddr>,
    pub config: PathBuf,
}

#[derive(Serialize, Deserialize, Clone, Debug)]
pub struct Clock {
    pos: (usize, usize),
    format: String,
    refresh: f32,
    utc: Option<bool>,
}

impl Clock {
    pub async fn run(self, send: mpsc::Sender<Vec<u8>>) {
        let mut interval = tokio::time::interval(Duration::from_secs_f32(self.refresh));
        interval.set_missed_tick_behavior(MissedTickBehavior::Skip);

        loop {
            interval.tick().await;
            let time = if self.utc == Some(true) {
                chrono::Utc::now().format(&self.format).to_string()
            } else {
                chrono::Local::now().format(&self.format).to_string()
            };
            let mut update = text_to_update(&time);
            update.set_position(self.pos.0, self.pos.1);
            let msg = update.to_message();
            send.send(msg).await;
        }
    }
}

#[derive(Serialize, Deserialize, Clone, Debug)]
pub struct Text {
    pos: (usize, usize),
    text: String,
    refresh: f32,
}

impl Text {
    pub async fn run(self, send: mpsc::Sender<Vec<u8>>) {
        let mut interval = tokio::time::interval(Duration::from_secs_f32(self.refresh));
        interval.set_missed_tick_behavior(MissedTickBehavior::Skip);

        loop {
            interval.tick().await;
            let mut update = text_to_update(&self.text);
            update.set_position(self.pos.0, self.pos.1);
            let msg = update.to_message();
            send.send(msg).await;
        }
    }
}

#[derive(Serialize, Deserialize, Clone, Debug)]
pub struct Clear {
    pos: (usize, usize),
    size: (usize, usize),
    refresh: f32,
}

impl Clear {
    pub async fn run(self, send: mpsc::Sender<Vec<u8>>) {
        let mut interval = tokio::time::interval(Duration::from_secs_f32(self.refresh));
        interval.set_missed_tick_behavior(MissedTickBehavior::Skip);

        loop {
            interval.tick().await;
            let mut update = Update::new();
            update.set_size(self.size.0, self.size.1);
            update.set_position(self.pos.0, self.pos.1);
            let msg = update.to_message();
            send.send(msg).await;
        }
    }
}

#[derive(Serialize, Deserialize, Clone, Debug)]
pub struct Fill {
    pos: (usize, usize),
    size: (usize, usize),
    refresh: f32,
}

impl Fill {
    pub async fn run(self, send: mpsc::Sender<Vec<u8>>) {
        let mut interval = tokio::time::interval(Duration::from_secs_f32(self.refresh));
        interval.set_missed_tick_behavior(MissedTickBehavior::Skip);

        loop {
            interval.tick().await;
            let mut update = Update::new();
            update.set_size(self.size.0, self.size.1);
            update.set_position(self.pos.0, self.pos.1);
            for row in 0..self.size.0 {
                for col in 0..self.size.1 {
                    update.set_bit(row, col, true);
                }
            }
            let msg = update.to_message();
            send.send(msg).await;
        }
    }
}

#[derive(Serialize, Deserialize, Clone, Debug)]
#[serde(tag = "type")]
pub enum Widget {
    Clock(Clock),
    Text(Text),
    Clear(Clear),
    Fill(Fill),
}

impl Widget {
    pub async fn run(self, send: mpsc::Sender<Vec<u8>>) {
        use Widget::*;
        match self {
            Clock(clock) => clock.run(send).await,
            Text(text) => text.run(send).await,
            Clear(clear) => clear.run(send).await,
            Fill(fill) => fill.run(send).await,
        }
    }
}

#[derive(Serialize, Deserialize, Clone, Debug)]
pub struct Screen {
    pub size: (usize, usize),
    pub server: Option<SocketAddr>,
}

#[derive(Serialize, Deserialize, Clone, Debug)]
pub struct Config {
    pub widgets: Vec<Widget>,
    pub screen: Screen,
}
