use itertools::Itertools;
use bitvec::prelude::*;
use bitvec::order::Lsb0;

pub mod text;
pub mod widgets;

#[derive(Clone, Debug)]
pub struct Update {
    pos: (usize, usize),
    size: (usize, usize),
    data: Vec<BitVec<Lsb0, u8>>,
}

impl Update {
    pub fn new() -> Update {
        Update {
            pos: (0, 0),
            size: (0, 0),
            data: Vec::new(),
        }
    }

    pub fn clear(&mut self) {
        self.data.iter_mut().for_each(|r| r.iter_mut().for_each(|mut i| *i = false));
    }

    pub fn set_size(&mut self, rows: usize, cols: usize) {
        self.size = (rows, cols);
        self.data.resize_with(rows, || BitVec::repeat(false, cols));
        self.data.iter_mut().for_each(|r| r.resize(cols, false));
    }

    pub fn set_position(&mut self, x: usize, y: usize) {
        self.pos = (x, y);
    }

    pub fn set_bit(&mut self, x: usize, y: usize, value: bool) {
        *self.data[x].get_mut(y).unwrap() = value;
    }

    pub fn set_byte(&mut self, x: usize, y: usize, byte: u8) {
        self.data[x].as_mut_raw_slice()[y] = byte;
    }

    pub fn to_message(&self) -> Vec<u8> {
        let mut out = vec![self.pos.0 as u8, self.pos.1 as u8, self.size.0 as u8, self.size.1 as u8];
        self.data.iter().for_each(|row| {
            out.extend_from_slice(row.as_raw_slice());
        });
        out
    }
}

